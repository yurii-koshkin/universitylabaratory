package org.labaratory.core;

/**
 * This class is basic FibonacciNumber implementation
 *
 * @author Yurii Koshkin
 */
public class FibonacciNumber {
    private double number = 1;
    private double previousNumber = 0;

    /**
     * This method returns current Fibonacci number
     *
     * @return Current Fibonacci number
     */
    public double getNumber() {
        return number;
    }

    /**
     * This method calculation next Fibonacci number
     */
    public void next() {
        double temp = number;
        number = number + previousNumber;
        previousNumber = temp;
    }
}
