package org.labaratory.utils;

import static java.lang.Math.*;

/**
 * This class contains extended math calculation methods.
 *
 * @author Yurii Koshkin
 */
public class CalculationUtils {
    /**
     * This method takes double value and check if it N*N*N-1
     * @param number Double number to check
     * @return True if number is N*N*N-1
     */
    public static boolean isSuitable(double number) {
        return cbrt(number - 1) % 1 == 0;
    }
}
