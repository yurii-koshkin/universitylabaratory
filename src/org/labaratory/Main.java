package org.labaratory;

import org.labaratory.core.FibonacciNumber;
import org.labaratory.utils.CalculationUtils;

import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter count of numbers: ");
        int numbersCount = sc.nextInt();

        FibonacciNumber fibonacciNumber = new FibonacciNumber();
        Set<Double> suitableNumbers = new HashSet<>();

        for (int i = 0; i < numbersCount; i++) {
            if (CalculationUtils.isSuitable(fibonacciNumber.getNumber())) {
                suitableNumbers.add(fibonacciNumber.getNumber());
            }
            System.out.println(fibonacciNumber.getNumber());
            fibonacciNumber.next();
        }

        System.out.println("Suitable numbers: " + suitableNumbers);
    }

}